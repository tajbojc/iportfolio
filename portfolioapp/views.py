from django.shortcuts import render,redirect
from django.contrib import messages
from portfolioapp.models import Contact
from django.http import HttpResponse

from django.utils.translation import gettext as _
from django.utils.translation import get_language, activate, gettext

# Create your views here.
def Home(request):
    trans = translate (language='bs')
    return render(request, 'indexdebian.html', {'trans': trans})




def translate(language):
    cur_language = get_language()
    try:
        activate(language)
        text = _('hello')
    finally:
        activate(cur_language)
    return text
    return render(request, "indexdebian.html")

def contact(request):
    if request.method=="POST":
        contact=Contact()
        name=request.POST.get('name')
        email=request.POST.get('email')
        subject=request.POST.get('subject')
        message=request.POST.get('message')
        contact.name=name
        contact.email=email
        contact.subject=subject
        contact.message=message
        contact.save()
        return HttpResponse("<h1>Thanks for Contacting Me</h1>")

    return render(request,"indexdebian.html")

    
